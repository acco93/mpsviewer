package controller;

import model.Model;
import parser.Parser;
import view.View;

public class Controller {

	private View view;

	public Controller() {
		view = new View(this);
	}

	public void load(String path) {

		view.showLoadingMessage();
		
		new Thread(()-> {
			Parser parser = new Parser(path);
			Model model = parser.parse();
			view.disposeLoadingMessage();
			view.render(model);	
		}).start();
		

	}

}
