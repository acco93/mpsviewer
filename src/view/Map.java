package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import model.Constraint;
import model.Model;
import model.Variable;

public class Map extends JPanel implements KeyListener {

	private Model model;

	private int span = 16;
	private Point currentPoint = new Point(-1, -1);
	private int toLeft = 0;
	private int toTop = 0;
	private String textPoint;

	public Map() {
		this.setBackground(Color.BLACK);

		
		this.addKeyListener(this);
		this.setFocusable(true);
		
		MouseAdapter ma = new MouseAdapter() {

			private Point startDragPoint;

			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				int notches = e.getWheelRotation();
				if (notches < 0) {
					zoomIn();
				} else {
					zoomOut();
				}
				repaint();
			}

			@Override
			public void mouseDragged(MouseEvent e) {
				if (SwingUtilities.isMiddleMouseButton(e)) {

					int x = (int) Math.floor(e.getPoint().x / span) - toLeft;
					int y = (int) Math.floor(e.getPoint().y / span) - toTop;

					int xOffset = x - startDragPoint.x;
					int yOffset = y - startDragPoint.y;

					toLeft += xOffset;
					toTop += yOffset;

					repaint();
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {

				int x = (int) Math.floor(e.getPoint().x / span) - toLeft;
				int y = (int) Math.floor(e.getPoint().y / span) - toTop;

				if (SwingUtilities.isLeftMouseButton(e)) {

				} else if (SwingUtilities.isRightMouseButton(e)) {

				} else if (SwingUtilities.isMiddleMouseButton(e)) {

					startDragPoint = new Point(x, y);

				}

				grabFocus();
				repaint();
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				startDragPoint = null;
			}

		};

		this.addMouseListener(ma);
		this.addMouseMotionListener(ma);
		this.addMouseWheelListener(ma);

	}

	public void render(Model model) {
		this.model = model;
		repaint();
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);

		if (this.model == null) {
			return;
		}

		Graphics2D g2 = (Graphics2D) g;
		/*
		 * g2.setStroke(new BasicStroke(1));
		 * g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
		 * RenderingHints.VALUE_ANTIALIAS_ON);
		 */

		int rowBegin = Integer.max(0, -toTop);
		int rowVisibleElements = this.getHeight() / span;
		int rowEnd = Integer.min(this.model.getConstraints().size(), rowBegin + rowVisibleElements);

		int columnBegin = Integer.max(0, -toLeft);
		int columnVisibleElements = this.getWidth() / span;
		int columnEnd = Integer.min(this.model.getVariables().size(), columnBegin + columnVisibleElements);

		for (int row = rowBegin; row < rowEnd; row++) {

			Constraint constraint = this.model.getConstraints().get(row);

			switch (constraint.getType()) {
			case E:
				g.setColor(Color.RED);
				break;
			case G:
				g.setColor(Color.blue);
				break;
			case L:
				g.setColor(Color.green);
				break;
			case N:
				g.setColor(Color.WHITE);
				break;
			default:
				break;

			}

			for (int column = columnBegin; column < columnEnd; column++) {
				Variable variable = this.model.getVar().get(column);
				if (this.model.getConstraintMap().get(constraint.getName()).contains(variable)) {
					g.fillRect(this.xValueOf(column), this.yValueOf(row), span, span);
				}
			}
		}

	}

	@Override
	public Dimension getPreferredSize() {
		return this.getSize();
	}

	private void zoomIn() {
		if (this.span == 40) {
			return;
		}
		this.span++;
	}

	private void zoomOut() {
		if (this.span == 1) {
			return;
		}
		this.span--;
	}

	private int xValueOf(int x) {
		return x * span + toLeft * span;
	}

	private int yValueOf(int y) {
		return y * span + toTop * span;
	}
	
	public void keyPressed(KeyEvent e) {
		int code = e.getKeyCode();
		switch (code) {
		case KeyEvent.VK_LEFT:
			this.toLeft++;
			break;
		case KeyEvent.VK_RIGHT:
			this.toLeft--;
			break;
		case KeyEvent.VK_UP:
			this.toTop++;
			break;
		case KeyEvent.VK_DOWN:
			this.toTop--;
			break;
		case KeyEvent.VK_MINUS:
			this.zoomOut();
			break;
		case KeyEvent.VK_PLUS:
			this.zoomIn();
			break;
		case KeyEvent.VK_SHIFT:

			break;
		}

		repaint();
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
