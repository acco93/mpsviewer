package view;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import controller.Controller;
import model.Model;

public class View extends JFrame {

	private static final int HEIGHT = 800;
	private static final int WIDTH = 1200;
	private Map map;
	private JDialog dialog;
	private JPanel loadingPanel;
	private MenuBar menuBar;

	public View(Controller controller) {
		this.setTitle(".mps file viewer");
		this.setSize(WIDTH, HEIGHT);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

		loadingPanel = new JPanel();
		loadingPanel.setBackground(Color.BLACK);
		JLabel parsingLabel = new JLabel("Please wait ... ");
		parsingLabel.setOpaque(true);
		parsingLabel.setBackground(Color.red);
		loadingPanel.add(parsingLabel);

		menuBar = new MenuBar(controller);
		this.setJMenuBar(menuBar);

		this.setLayout(new BorderLayout());

		map = new Map();

		this.add(map, BorderLayout.CENTER);

		this.setVisible(true);
	}

	public void render(Model model) {
		this.map.render(model);
	}

	public void showLoadingMessage() {
		SwingUtilities.invokeLater(() -> {
			this.getContentPane().add(loadingPanel, BorderLayout.SOUTH);
			this.menuBar.lock();
			this.invalidate();
			this.validate();
		});
	}

	public void disposeLoadingMessage() {
		SwingUtilities.invokeLater(() -> {
			this.menuBar.unlock();
			this.getContentPane().remove(loadingPanel);
			this.invalidate();
			this.validate();
		});
	}

}
