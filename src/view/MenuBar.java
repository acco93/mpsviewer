package view;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import controller.Controller;

public class MenuBar extends JMenuBar {

	private JMenuItem importItem;

	public MenuBar(Controller controller) {

		JMenu fileMenu = new JMenu("File");
		importItem = new JMenuItem("Load instance");

		importItem.addActionListener((ActionEvent e) -> {

			JFileChooser fileChooser = new JFileChooser();
			if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				controller.load(file.getPath());
			}

		});

		this.add(fileMenu);
		fileMenu.add(importItem);

	}

	public void unlock() {
		this.importItem.setEnabled(true);
	}

	public void lock() {
		this.importItem.setEnabled(false);
	}

}
