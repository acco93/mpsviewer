package model;

public class Variable {

	private String name;
	private String constraintName;
	private double coefficient;
	private boolean integer;

	public void setName(String string) {
		this.name = string;
	}

	public void setConstraintName(String string) {
		this.constraintName = string;
	}

	public void setCoefficient(String string) {
		this.coefficient = Double.parseDouble(string);
	}

	public String getName() {
		return name;
	}

	public String getConstraintName() {
		return constraintName;
	}

	public double getCoefficient() {
		return coefficient;
	}

	public void setInteger(boolean isInteger) {
		this.integer = isInteger;
	}

	public boolean isInteger() {
		return integer;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Variable other = (Variable) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	
	
}
