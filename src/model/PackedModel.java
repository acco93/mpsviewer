package model;

import model.Constraint.ConstraintType;

public class PackedModel {

	private int rows;
	private int columns;
	private double[][] matrix;
	private ConstraintType[] sense;

	public PackedModel(int rows, int columns, double[][] matrix, ConstraintType[] sense) {
		this.rows = rows;
		this.columns = columns;
		this.matrix = matrix;
		this.sense = sense;
	}

	public int getRows() {
		return rows;
	}

	public int getColumns() {
		return columns;
	}

	public double[][] getMatrix() {
		return matrix;
	}

	public ConstraintType[] getSense() {
		return sense;
	}

}
