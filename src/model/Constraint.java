package model;

public class Constraint {

	public enum ConstraintType {
		N, E, G, L;
	}

	private String name;
	private ConstraintType type;


	public void setType(String string) {
		switch (string) {
		case "N":
			this.type = ConstraintType.N;
			break;
		case "E":
			this.type = ConstraintType.E;
			break;
		case "G":
			this.type = ConstraintType.G;
			break;
		case "L":
			this.type = ConstraintType.L;
			break;
		}
	}

	public void setName(String string) {
		this.name = string;
	}

	public String getName() {
		return name;
	}

	public ConstraintType getType() {
		return type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Constraint other = (Constraint) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	
	
}
