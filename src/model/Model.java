package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Model {

	private String name;

	private Map<String, Set<Variable>> constraintMap;
	private Map<String, Constraint> constraints;
	private List<Constraint> enc;
	private List<Variable> var;
	private Set<Variable> variables;
	private Set<Variable> integerVariables;

	public Model() {
		this.constraintMap = new HashMap<>();
		this.constraints = new HashMap<>();
		this.variables = new HashSet<>();
		this.integerVariables = new HashSet<>();
		this.enc = new ArrayList<>();
		this.var = new ArrayList<>();
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void add(Constraint constraint) {
		if (constraint.getName().length() == 0) {
			System.out.println("Unnamed constraint skipped.");
			return;
		}

		this.constraintMap.put(constraint.getName(), new HashSet<>());
		this.constraints.put(constraint.getName(), constraint);
		this.enc.add(constraint);
	}

	public void add(Variable variable) {

		Set<Variable> list = this.constraintMap.get(variable.getConstraintName());

		if (list == null) {
			System.out.println("Child variable skipped.");
			return;
		}

		list.add(variable);

		boolean newVariable = variables.add(variable);
		if (newVariable) {
			this.var.add(variable);

		}

		if (variable.isInteger()) {
			this.integerVariables.add(variable);
		}

	}

	public Set<Variable> getVariables() {
		return variables;
	}

	public Set<Variable> getIntegerVariables() {
		return integerVariables;
	}

	public int constraintsNum() {
		return this.constraintMap.size();
	}

	public int variablesNum() {
		return this.variables.size();
	}

	public List<Constraint> getConstraints() {
		return enc;
	}

	public List<Variable> getVar() {
		return var;
	}

	public Map<String, Set<Variable>> getConstraintMap() {
		return constraintMap;
	}

}
