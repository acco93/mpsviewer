package parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

import model.Constraint;
import model.Model;
import model.Variable;

public class Parser {

	private static final int F0 = 0;
	private static final int F1 = 1;
	private static final int F2 = 2;
	private static final int F3 = 3;
	private static final int F4 = 4;
	private static final int F5 = 5;

	private static final String NAME = "NAME";
	private static final String ROWS = "ROWS";
	private static final String COLUMNS = "COLUMNS";
	private static final String RHS = "RHS";
	private static final String MARKER = "'MARKER'";
	private static final String INTORG = "'INTORG'";
	private static final String INTEND = "'INTEND'";
	private Scanner scanner;
	private File file;
	private String currentSection;
	private boolean integerSection;

	public Parser(String filePath) {
		// open the file
		file = new File(filePath);

	}

	/*
	 * In an .mps file there may be at most 6 fields:
	 *
	 * - field 1 is in columns 2-3 - field 2 is in columns 5-12 - field 3 is in
	 * columns 15-22 - field 4 is in columns 25-36 - field 5 is in columns 40-47 -
	 * filed 6 is in columns 50-61
	 * 
	 */

	public Model parse() {

		if (!file.exists()) {
			System.out.println("File doesn't exists.");
			return null;
		}

		Model model = new Model();

		try {

			scanner = new Scanner(file);

			// parse the name header
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				//System.out.println(line);
				String[] tokens = line.split("\\s+");
				//System.out.println(Arrays.deepToString(tokens));

				if (tokens[F0].startsWith(NAME)) {
					this.currentSection = NAME;
				} else if (tokens[F0].startsWith(ROWS)) {
					this.currentSection = ROWS;
					line = scanner.nextLine(); // skip the line with ROWS and prepare new tokens
					tokens = line.split("\\s+");
				} else if(tokens[F0].startsWith(COLUMNS)) {
					this.currentSection = COLUMNS;
					line = scanner.nextLine(); // skip the line with COLUMNS and prepare new tokens
					tokens = line.split("\\s+");
				} else if(tokens[F0].startsWith(RHS)) {
					this.currentSection = RHS;
				}

				parseCurrentLine(model, line, tokens);

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return model;
	}

	private void parseCurrentLine(Model model, String line, String[] tokens) {

		switch (this.currentSection) {
		case NAME:
			model.setName(tokens[F1]);
			//System.out.println(model.getName());
			break;
		case ROWS:
			Constraint constraint = new Constraint();
			constraint.setType(tokens[F1]);
			constraint.setName(tokens[F2]);
			model.add(constraint);
			//System.out.println(Arrays.deepToString(tokens));
			break;
		case COLUMNS:
			// in any time an integer group may be declared
			if(tokens[F2].startsWith(MARKER) && tokens[F3].startsWith(INTORG)) {
				this.integerSection = true; 
				return;
			} 
			if(tokens[F2].startsWith(MARKER) && tokens[F3].startsWith(INTEND)) {
				this.integerSection = false;
				return;
			} 
			
			Variable variable = new Variable();
			variable.setName(tokens[F1]);
			variable.setConstraintName(tokens[F2]);
			variable.setCoefficient(tokens[F3]);
			variable.setInteger(this.integerSection);
			
			model.add(variable);
			
			break;
		default:
			break;
		}

	}

}
